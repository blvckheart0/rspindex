import { Component } from '@angular/core';
import {TuiIslandModule} from '@taiga-ui/kit';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'RspIndex';
}
